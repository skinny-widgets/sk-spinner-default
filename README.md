# Skinny Widgets Spinner for Default Theme


spinner element

```
npm i sk-spinner sk-spinner-default --save
```

then add the following to your html

```html
<sk-config
    theme="default"
    base-path="/node_modules/sk-core/src"
    theme-path="/node_modules/sk-theme-default"
></sk-config>
<sk-button id="skButton" button-type="primary">Show Dialog</sk-button>

<sk-spinner id="skSpinner"></sk-spinner>

<script type="module">
    import { SkSpinner } from '/node_modules/sk-spinner/sk-spinner.js';
    import { SkButton } from '/node_modules/sk-button/sk-button.js';

    customElements.define('sk-spinner', SkSpinner);
    customElements.define('sk-button', SkButton);

    skButton.addEventListener('click', (event) => {
        skSpinner.dispatchEvent(new CustomEvent('toggle'));
    });
</script>
```

#### template

id: SkSpinnerTpl