
import { SkSpinnerImpl }  from '../../sk-spinner/src/impl/sk-spinner-impl.js';

export class DefaultSkSpinner extends SkSpinnerImpl {

    get prefix() {
        return 'default';
    }

    get suffix() {
        return 'spinner';
    }

    afterRendered() {
        super.afterRendered();
        this.mountStyles();
    }
}
